# srtcon - SilverRail StrongSwan vpn connection

srtcon is a bash script that receives a particular option to interact with ipsec. You can start, stop or reset.

## Installation

### Bash script as extension

Here is a description of how to create a bash script and make it a comman such as 'whereis', 'cat' or 'grep'.

1. Create a script such as srtcon.sh
2. Change permissions to be able to execute the script: `chmod +x srtcon.sh`
   1. **Optional**: Create and move to `/opt/scripts/` folder.
3. Create an alias on `.bash_aliases` such as follows: `alias wscon='/path/to/dir/srtcon.sh'`
   1. **Note**: if `.bash_aliases` doesn't exist, create it at `/home/<user>/`.
4. Source `.bashrc`: `source ~/.bashrc`

### Running aliases as sudo

ipsec requires `sudo` to run. Aliases can be run as sudo by adding an alias to sudo.

1. Create an alias to sudo on `.bash_aliases` such as: `alias sudo='sudo '`
   1. **Note**: Reference: [AskUbuntu Answer](https://askubuntu.com/a/22043)

### Adding autocomplete to bash script

You can add autocomplete by using bash_autocomplete as shown below:

1. Create a new file at `/etc/bash_autocomplete.d/` such as `srtcon_complete`
   1. Check reference on what each part of this file means [here](https://askubuntu.com/a/345150)

## Usage

To use the script you must have run all the installation. After all installation steps are complete you may use the script as follows:

1. To start dev-vpn: `sudo srtcon start`
2. To stop dev-vpn: `sudo srtcon stop`
3. To restart dev-vpn: `sudo srtcon restart`