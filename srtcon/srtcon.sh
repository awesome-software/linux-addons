#!/bin/bash
option=$1

start_ipsec () {
    ipsec up dev-vpn
}

stop_ipsec () {
    ipsec down dev-vpn
}

case "$option" in

    "") echo "Usage: srtcon [start | stop | restart]"
       ;;

    "start") echo "Starting ipsec"
            start_ipsec
	    ;;
    "stop") echo "Stopping ipsec"
           stop_ipsec
	   ;;
    "restart") echo "Restarting ipsec"
           stop_ipsec
	   start_ipsec
	   ;;
    *) echo "Option $option doesn't exist"
     ;;
esac
