# wscon - Windscribe conditional connect

wscon is a bash script that checks if Windscribe VPN is connected. If it is it disconnects, if it not it connects.

## Bash script as extension

Here is a description of how to create a bash script and make it a comman such as 'whereis', 'cat' or 'grep'.

1. Create a script such as wscon.sh
2. Change permissions to be able to execute the script: `chmod +x wscon.sh`
   1. **Optional**: Create and move to `/opt/scripts/` folder.
3. Create an alias on `.bash_aliases` such as follows: `alias wscon='/path/to/dir/wscon.sh'`
   1. **Note**: if `.bash_aliases` doesn't exist, create it at ``/home/<user>/``.
4. Source `.bashrc`: `source ~/.bashrc`