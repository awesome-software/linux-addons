#/bin/bash -xe
if [ ! $( windscribe status | grep DISCONNECTED) ]; then
    echo "Disconnecting.."
    windscribe disconnect
else
    echo "Connecting..."
    windscribe connect CO
fi
